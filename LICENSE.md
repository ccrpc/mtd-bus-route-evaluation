Sustainable Neighborhoods Bus Route Evaluation for the Champaign-Urbana Mass Transit District by the [Champaign
Urbana Urbanized Area Transportation Study](https://cuuats.org/) is licensed
under a [Creative Commons Attribution 4.0 International
License](https://creativecommons.org/licenses/by/4.0/).

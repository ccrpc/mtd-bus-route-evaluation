---
title: Scenarios
draft: false
weight: 20
menu: main
---

The routes suggested by Nelson/Nygaard mirror the existing transit network almost one-to-one, with each current route having a counterpart in the proposed system. In most cases, those routes are more streamlined, with fewer deviations and less service to lower ridership areas. MTD and RPC staff took the weekday daytime route system and divided it into four stages to create the scenarios.

The scenarios are designed to be considered cumulatively. The first scenario will look at two of the larger route suggestions being implemented while the rest of the system runs under the current route structure. For the second scenario, a few more routes will be replaced, until the fourth scenario, which shows a system with full adoption of the Nelson/Nygaard suggestions.

MTD’s service is divided into “campus” routes, serving the UIUC campus area, and “community routes”, serving the surrounding community. In the scenarios proposed by MTD, only community routes were included in the scenarios. The campus area has all-around higher pedestrian Access Scores; since transit Access Scores cannot be lower than the pedestrian Access Score, it would not be evident from the scores whether transit access was adequate or lacking. In the community, particularly among the analysis zones outlined later in this document, a transit score of 50 is better than a score of 25, a score of 85 is better than 50, etc. If the pedestrian score is 90, though, and the transit score cannot be lower than the pedestrian score, then many scores would look similar with no way of differentiating between them. For this reason, areas with lower walkability are better suited for analyzing changes in transit access.

As a note, MTD routes are currently named with a number and a color. The proposed routes are named with a number and the street or neighborhood that they serve most directly. The overall structure of the two systems are similar, with each route sharing a number with a counterpart. As a note, the current 5 Green route also serves Green Street the most prominently, and is therefore called the "5 Green" under both naming conventions.

## Scenario 1: 5 Green/15 Green

### Existing Route Structure

The current 5 Green route runs from West Champaign to the east edge of Urbana, serving Downtown Champaign, the university campus, and Downtown Urbana. The route is the longest and among the most productive in the system, but possibly the most intricate and complex. 

Currently, three routes operate under the larger 5 Green umbrella. The default 5 Green serves, from west to east:

* Round Barn Road (serving a shopping center with access to restaurants, services, a grocery store, and a pharmacy)
* Illinois Terminal (the bus transfer center in Downtown Champaign)
* University of Illinois campus
* Lincoln Square (the bus transfer center in Downtown Urbana)
* The Brookens Building (the Champaign County/Regional Planning Commission offices)

All of these routes also serve residential areas. On the east and west ends of each route, these include predominantly lower income neighborhoods.

The 5 Green Hopper serves the same major features as the 5 Green. However, rather than reinforcing frequencies along the core of the route by serving a shorter version of the route, the Green Hopper mostly deviates to serve other locations. On the western terminus of the route, the Green Hopper serves Parkland College instead of the Holiday Park residential neighborhood. While the eastern terminus still serves the Brookens Building, the Green Hopper serves the residential areas south of the building, where the traditional 5 Green serves the residential neighborhoods north and east of it.

The final variation, the 5 Green Express, follows a largely different route. It goes through the University campus area, skips Downtown Champaign, proceeds to Round Barn Road, and then serves select neighborhoods in West and Southwest Champaign. The Green Express also only runs during peak hours, traveling west towards campus in the morning, and back towards the residential areas in the afternoon.

<iframe src="https://maps.ccrpc.org/snt-existing-green/" width="100%" height="600" allowfullscreen="true"></iframe>

### Proposed Route Structure

Under the proposed route structure, the 5 Green is broken into the 5 Green, which effectively replaces the Green Hopper, and the 15 Green, which largely keeps the current Green routing. The 5 Green follows essentially the same routing between Parkland College and Lincoln Square. From there, it continues going eastward, but in a generally more streamlined manner, with fewer deviations into residential neighborhoods. The route continues more or less straight down Washington Street, serves Brookens. At the eastern terminus, the 5 Green goes north through the Scottswood neighborhood and, rather than ending in a loop, ends at the Urbana Walmart.

The proposed 15 Green is more or less unchanged from the existing 5 Green, continuing to serve the Holiday Park neighborhood at the western end of the route, and deviating further into neighborhoods to serve the Kinch neighborhood on the eastern end.

<iframe src=" https://maps.ccrpc.org/snt-proposed-green/" width="100%" height="600" allowfullscreen="true"></iframe>

## Scenario 2: 2 Florida/10 Springfield/11 Philo + Northeast Connect (+ Scenario 1)

### Existing Route Structure

Scenario 2 involves changing three existing routes: the 2 Red, 10 Gold, and 11 Ruby. The 2 Red is currently one of MTD’s most circuitous routes, starting in north Champaign, going through Downtown Champaign to campus, to south Urbana, then back north to Downtown Urbana.  The route provides service to Market Place Mall, the public health building, Downtown Champaign, UIUC campus, the Meijer in Urbana, and Downtown Urbana.

The 10 Gold serves Northeast Urbana before going south to Downtown Urbana, then serving the UIUC campus area before moving on to serve the residential areas in South Champaign. The Gold Hopper serves the area between Downtown Urbana and the western edge of campus, reinforcing route frequency.

The 11 Ruby stands to provide one of the biggest changes to accessibility of all of the proposed routes. The Ruby currently serves Northeast Urbana, running a large loop north of the interstate. The route is designed to pick up passengers from these predominantly residential areas, then bring them to Downtown Urbana and back. During weekdays, the Ruby only runs during peak hours, though it does run more consistently on evenings and weekends. 

<iframe src=" https://maps.ccrpc.org/snt-existing-scenario-2/" width="100%" height="600" allowfullscreen="true"></iframe>

### Proposed Route Structure

Following the route recommendations, the 2 Red (as the 2 Florida) will look fairly similar, except that instead of going to Downtown Urbana, trips will end and begin at the Meijer in Southeast Urbana. From a route design standpoint, this means that each trip is making a more straightforward trip northwest or southeast, without doubling back for the last leg of the trip.

The 10 Springfield similarly follows the routing of the current 10 Gold, but starts in Downtown Urbana, cutting off the section of the route that goes into Northeast Urbana.

The 11 Philo, replacing the 11 Ruby, effectively takes the two sections removed from the 2 Red and 10 Gold and combines them. The route will now start in Northeast Urbana, travel to Downtown Urbana, then travel south towards the Urbana Meijer.

Everything in Northeast Urbana north of the interstate that is currently served by the 10 Gold and 11 Ruby will be served by a new demand-response service called the Northeast Connect. The Northeast Connect will pick up passengers and bring them to Lincoln Square to facilitate connections to other transit routes.

<iframe src=" https://maps.ccrpc.org/snt-proposed-scenario-2/" width="100%" height="600" allowfullscreen="true"></iframe>

## Scenario 3: 6 University/14 Fields/16 Mattis + Southwest Connect (+Scenarios 1 and 2)

### Existing Route Structure

The third scenario focuses on the route structure in West Champaign. The 6 Orange travels east and west between East Urbana and West Champaign. The main role of the 6 Orange in the system is serving the medical facilities along University Ave, such as OSF and Carle Hospital. The western terminus of the route is currently the Round Barn Road transfer center in Champaign, with peak trips traveling further west to serve Plastipak, a local industrial employer.

The current 14 Navy was designed to serve the Carle at the Fields medical complex in Southwest Champaign. The route starts in Downtown Champaign, goes south through the edge of the UIUC campus area, then goes straight down Windsor to serve the YMCA and Carle at the Fields. During peak hours, the trips extend west to serve the Trails at Brittany and the surrounding residential subdivisions.

Finally, the 16 Pink currently runs in a loop serving West and Central Champaign. The 16 Pink has some of the lowest ridership in MTD’s system, currently designed to provide coverage for the residential areas on the fringes of the community. In addition to the residential areas on the far western edge of the urban area, the 16 Pink also serves Parkland College, Round Barn Road, the Christie Clinic, and the YMCA.

<iframe src=" https://maps.ccrpc.org/snt-existing-scenario-3/" width="100%" height="600" allowfullscreen="true"></iframe>

### Proposed Route Structure

Under the newly designed routes, the 6 Orange becomes the 6 University, and looks very similar. The key difference is that the 6 Orange continues west to serve the Cobblefield subdivision in West Champaign. The route change was made to compensate for the changes to the 16 Pink, which no longer serves that western edge of Champaign.

The 14 Fields route similarly looks very similar to the 14 Navy, starting in Downtown Champaign and continuing south through campus to Windsor Road. However, at the intersection of Windsor Road and Mattis Avenue, the Navy turns south to Curtis Road. From there, the route serves the Carle facility on Curtis, the Cherry Hills subdivision (which is currently served by the 5 Green Express, which will be discontinued under the new route system), then continues to Carle at the Fields.

It bears special noting that the proposed routing of the 14 Fields offers fairly substantial improvements to access. The Cherry Hills subdivision is currently only served during peak hours, and Carle on Curtis is not served at all. However, Carle on Curtis is currently not served because it is ineligible for annexation into MTD’s taxing district, which is a barrier to service that will need to be addressed.

The 16 Pink sees the largest transformation of the Champaign routes. Rather than running as a loop, it is restructured to provide much more substantial north-south service. The route starts at Carle at the Fields and proceeds to Mattis Avenue, Round Barn Road, and Parkland College following the current routing. However, from there, the 16 Mattis continues north, over I-74, to serve the Walmart in Champaign.

Southwest Champaign is currently served by a demand-response service called the West Connect. That service area is expanded to provide service to areas that would not be served under the new structure of the 16 Mattis.

<iframe src=" https://maps.ccrpc.org/snt-proposed-scenario-3/" width="100%" height="600" allowfullscreen="true"></iframe>

## Scenario 4: 1 Neil, 3 Garden Hills, 4 John, 7 Douglass, and 9 Duncan (+ Scenarios 1, 2, and 3)

<iframe src=" https://maps.ccrpc.org/snt-existing-scenario-4/" width="100%" height="600" allowfullscreen="true"></iframe>

The final collection of routes generally have the most minor changes of the suggested route changes. For example, the 1 Yellow starts at the far northern end of Champaign and goes to the far southern end of Savoy. The 1 Neil serves all of the same major community resources and features, but takes straighter paths to serve them. This increases average passenger walking distances in some cases, but also has the potential of moving buses more quickly as the route becomes more streamlined.

The same is largely true for the 3 Garden Hills. The 3 Lavender has Illinois Terminal as its southern terminus, traveling north to the commercial area on North Prospect Avenue south of the interstate. From there, the route goes through the Garden Hills and Dobbins Downs neighborhoods, before continuing north to the Walmart in Champaign. The 3 Garden Hills follows a similar pattern, taking a quicker path along the edge of the commercial area on North Prospect Avenue rather than cutting directly through it, reducing delays and back-tracking.

The 4 Blue serves Illinois Terminal before going through the campus area. It turns westward from campus to serve the residential areas in Central Champaign along John Street. The route ends at Round Barn Road, which the 4 Blue serves turning south onto Mattis from John Street, west onto Kirby Avenue, and back north onto Crescent Drive. This loop serves Centennial Park, Centennial High School, Jefferson Middle School, and the residents of the neighborhood east of the park. The 4 John serves most of the same features, but makes a loop at its western terminus to provide more direct service to one of the residential areas east of Centennial Park, which it now approaches more directly.

The 7 Grey travels from the Walmart in East Urbana to Lincoln Square in Downtown Urbana. It continues west through the King Park and Douglass Park neighborhoods, two predominantly Black neighborhoods that collectively straddle the border between Urbana and Champaign. The 7 Grey serves Illinois Terminal in Downtown Champaign, then travels west to Prospect Avenue, taking Vine Street to McKinley Avenue to Bradley Avenue to avoid the congestion of the North Prospect commercial area. Once on Bradley Avenue, the 7 Grey serves the Kraft plant, the Williamsburg neighborhood, and ends at Parkland College.  As the 7 Douglass, the route remains mostly unaltered with only a few changes. The new routing goes directly from Prospect Avenue onto Bradley Avenue, reducing the number of turns and serving the commercial areas more directly. It no longer deviates into the Williamsburg neighborhood, but now continues west past Parkland to serve the Boulder Ridge subdivision previously served by the 16 Pink.

The 9 Brown currently runs in a large loop connecting Parkland College, Illinois Terminal, the UIUC campus area, and the residential neighborhoods in south-central Champaign.  The 9 Duncan similarly serves the same major resources as the 9 Brown, in addition to the commercial areas previously served by the 3 Lavender, but gives less direct service to the residential areas on the south part of the route. Where the 9 Brown routing loops through the neighborhoods and goes further south to Windsor to increase the directly serves areas, the 9 Duncan stays on Kirby, continuing the trend of increased walking distances to routes that then move more quickly and directly.

<iframe src=" https://maps.ccrpc.org/snt-proposed-scenario-4/" width="100%" height="600" allowfullscreen="true"></iframe>

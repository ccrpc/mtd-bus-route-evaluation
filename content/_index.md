---
title: Home
draft: false
bannerHeading: "Sustainable Neighborhoods Bus Route Evaluation for the Champaign-Urbana Mass Transit District"
bannerText: "Determining accessibility of potential route design scenarios"
---

# Sustainable Neighborhoods Bus Route Evaluation for the Champaign-Urbana Mass Transit District

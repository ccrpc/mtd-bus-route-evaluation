---
title: Introduction and Background
draft: false
weight: 10
menu: main
---

In 2019 and 2020, the Champaign-Urbana Mass Transit District employed a consultant to carry out a route analysis, assessing the continued suitability and opportunities for improvement in the current route structure. In the same window of time, the Champaign County Regional Planning Commission developed the Sustainable Neighborhoods Toolkit, a modeling suite that includes the Access Score. The goal of this report is to apply the methodology of the Access Score to the proposed route structure to assess the accessibility and equity impacts of the proposed system modifications.

## Background

The Champaign-Urbana Mass Transit District (MTD) provides urban public transportation service through Champaign, Urbana, and the University of Illinois at Urbana-Champaign campus. This combination of service for traditional permanent residents (“community” service) and routes designed for students and campus personnel (“campus” service) has created a uniquely efficient and productive transit system, providing over 11 million passenger trips annually pre-COVID.

<iframe src=" https://maps.ccrpc.org/snt-full-existing-route/" width="100%" height="600" allowfullscreen="true"></iframe>

In 2019, MTD applied for and received IDOT grant funding to conduct a route analysis and identify opportunities and system improvements. The system had seen relatively minor adjustments for decades prior. Downtown Champaign, Downtown Urbana, and the university campus are fixed points, so a route running through any combination of those hubs would remain efficient over time; however, development outside of the core had changed and a formal analysis could identify service gaps and opportunities for improvement.

The analysis also included an assessment of route design, including schedules. Transit service design can fall on a spectrum, balancing simplicity and ease of use on one end, and service that becomes more complicated for the sake of being more accommodating on the other end. MTD’s route system deviates to run through most community neighborhoods, with additional deviations inserted into the schedule at different times of the day or on certain trips. This minimizes passengers' walking distance and expands the general accessible service area, but also lengthens trip times and makes the service less immediately intuitive.

The funding was awarded in 2019 and, after a request for proposals, Nelson/Nygaard was selected to conduct the study. After a series of outreach events and stakeholder meetings, the final recommendations were delivered in October 2020. The recommendations largely centered on more streamlined routes, reinforcing core service, and implementing on-demand microtransit solutions to cover less productive, lower density, or otherwise harder to serve neighborhoods.

<iframe src=" https://maps.ccrpc.org/snt-full-proposed-route/" width="100%" height="600" allowfullscreen="true"></iframe>

In 2019, the Champaign County Regional Planning Commission (RPC) developed the Access Score model as part of the Sustainable Neighborhoods Toolkit. The Sustainable Neighborhoods Toolkit has the ability to take in formatted geospatial data and generate Levels of Traffic Stress (LTS) for pedestrians, bicyclists, and automobiles for each street segment in the community. The Access Score takes in that LTS score for each street segment, as well as a “destinations” layer with every community resource and feature. For each location in the Champaign-Urbana urbanized area, a score is calculated based on what resources are accessible, given the levels of traffic stress for the surrounding street network. With the advent of this toolkit, RPC applied for a State Planning and Research grant through the IDOT Office of Intermodal Project Implementation with the goal of determining the accessibility impacts of the Nelson/Nygaard route recommendations. The goal of the Sustainable Neighborhoods Bus Route Evaluation grant application was to leverage the Sustainable Neighborhoods Toolkit to assess the accessibility and equity impacts of the proposed transit system modifications. This will provide MTD with neighborhood-level insights into the impacts of the proposed changes, allowing them to develop an effective implementation plan.

## Access Score

The Access Score is a number calculated on a scale between 0 and 100. The model calculates a level of traffic stress between 1 and 4 for pedestrians, bicyclists, and motorists, creating a different score for each of the three modes. Each street segment is then assigned a point value based on the length of the segment and the level of traffic stress. For example, a street segment 100 feet long with a pedestrian stress score of 3 would have a value of 300 points.

Every mode is given a budget of points used to determine the Access Score. For example, pedestrian Access Scores are calculated using a budget of 15,840. For every segment in the community, a network is created showing how far a pedestrian can go in any direction, using the point value of each segment as compared to that travel mode’s budget of points.

Once that accessible area is determined for each segment, the model takes in a file of community resources. Resources are sorted into ten categories:

* Grocery stores
* Health facilities
* Arts and entertainment
* Public facilities
* Restaurants
* Jobs
* Schools
* Retail
* Services
* Parks

Each segment generates one Access Score for each type of resource and each travel mode, e.g. a pedestrian grocery score, a pedestrian health score, a bicycle grocery score, etc. The final score indicates the percentage of the point budget remaining once the trip is made. For example, if a given segment has a pedestrian grocery score of 80, that means that stress values of the segments required to access a grocery score only equaled 20% of the total budget.

The budgets are intended to represent the higher end of what the public would consider a “reasonable” trip. This is important, because any number greater than zero can be accomplished within the definition of “a reasonable trip.” For the Access Score, a score of zero means that the whole budget was considered and the destination was not accessible. A score of 5 may mean that 95% of the point budget was used, but that budget is designed to represent a reasonable trip. This is an important distinction, since many scales are designed with score under 50 or 60 to indicate a failure. However, a score of 90 is more likely to be easily accessible under any circumstances, where a score of 5 would potentially not be possible due to the length of the trip in the case of rain, extreme cold, etc.

The map below shows aggregated pedestrian Access Scores as an example. The bright blue area in the urban core would fall on the higher end of the Access Score, since so many resources would be available to pedestrians in the center of the community. The fringe of the community has scores closer to zero, as not every resource will be available within walking distance.

<image src="access_score.PNG"
  attr="Sample Access Score"
  position="full">

For transit specifically, the model is modified to calculate the Access Score using trip time as the chief metric. Most large transit systems publish their schedules in a data format called a general transit feed specification, or GTFS. The GTFS is a standardized format that includes information on each transit stop, how trips are put together, etc.

For each street segment, the Access Score can take the information in that GTFS feed and identify nearby bus stops and schedule information. With that information, the model assesses every possible transit trip that can be taken from that segment, what resources can be reached, and how long it will take to reach them. Trips are considered to be accessible if they can be completed within one hour. Because all transit trips involve walking, transit access is calculated in conjunction with the pedestrian Access Scores; however, the transit score can only improve on the pedestrian score. If a street segment has a pedestrian score of 70, the transit score may be higher than 70, but it cannot be lower, even if there is no transit access.

To analyze the impacts of the routes proposed to MTD by Nelson Nygaard, GTFS feeds will be generated to reflect four different service scenarios implementing different parts of Nelson Nygaard’s recommendations. Those GTFS feeds will be used to calculate new Access Scores, which can be compared against the existing conditions to determine overall impacts on accessibility.

---
title: Conclusion
draft: false
weight: 40
menu: main
---

The route structure proposed by Nelson Nygaard keeps the same basic framework as MTD’s existing route structure. The proposed routes are streamlined, minimizing deviations and backtracking, with a prioritization of straighter paths. With this design strategy, routes can be expected to move more quickly, while increasing walking distances to and from bus stops. Because the Access Score is able to consider both pedestrian distances and traffic stress, as well as transit trip times, it is an ideal tool to weigh the costs and benefits of this type of change.

Due to the nature of the Access Score, focusing performance evaluation on low-income neighborhoods and areas on the edges of the community is a key strategy for a successful study. As these are areas with inherently lower pedestrian Access Scores, the absence or presence of transit access has the ability to change the scores much more dramatically. Since the model frames transit access as being an additive to pedestrian access, areas with higher pedestrian access may not reflect transit access as cleanly.

## Next Steps

This report marks the completion of Task 1 of the report. Task 2 represents the actual analysis of the route structure using the Access Score. Using the information from MTD, a GTFS feed will be generated for each of the desired scenarios. MTD and RPC staff will coordinate on any changes to stop placement, determining route frequency, planned deviations, etc. Once the GTFS feed is finalized, it will be used as an input for the Access Score to generate new scores. Based on the analysis zones, scores will be aggregated to evaluate changes.

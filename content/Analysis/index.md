---
title: Analysis
draft: false
weight: 30
menu: main
---

According to MTD’s service request feedback received between September 2018 and September 2023, there are a few key themes that continue to arise regarding how the service meets customer needs. Service request feedback is collected from community members contacting MTD by phone or email and requesting new transit service or changes to existing service. There were 560 such requests made during this period. This feedback will be used to identify geographic areas and resources appropriate for performance evaluation.

A recurring theme is the span of service, particularly as it relates to evening and Sundays. This is a definite need but is constrained by staffing and budgetary needs. Most of the requests are unrelated, covering disparate locations and times. The chief exceptions are a general request for extended Sunday service.

One of the most recurring themes was a request for service to the Carle facility at Curtis Road. It is currently not served by MTD’s fixed-route service, due the facility being separated from the rest of the urban area by agricultural land that is not eligible to be annexed. However, that is projected to change under the design of the proposed 14 Fields route. There was also one request for service to the southern edge of the Carle at the Fields complex, which is also addressed by the 14 Fields.

There was a general request for service in Southwest Champaign, which took place in two basic categories. The first was a lack of satisfaction with the Green Express, which currently provides service from Southwest Champaign into campus in the morning peak, then provides return trips during the afternoon peak. The lack of mid-day, evening, and weekend service was a recurring theme, though the 14 Fields would provide full weekday service to Cherry Hills, the subdivision where most of that feedback was referring.

The second concern from Southwest Champaign was the lack of service on the western edge of that area. Many of those subdivisions were annexed in recent years and communicated a request for service. As it stands, there are some subdivisions served by the 14 Navy during peak hours, and the 16 Pink provides service along Staley Road, the main arterial running through that neighborhood, but for community members who live in certain subdivisions, the neighborhood design can make further service difficult. MTD offers a demand-response van service called the West Connect designed to pick up community members in that area, but it was largely suspended due to COVID-related staffing shortages. The West Connect is part of the suggested service, which will address those needs, and the proposed zone is expanded to account for areas that will receive less service due to other changes.

In North Champaign, particularly the Ashland Park subdivision and the area surrounding the FedEx facility, there were requests for more service. The current service provided is peak only service, which is expected to continue under the proposed route structure.

In Northeast Urbana, there was feedback regarding the structure of the 10 Gold route, which runs an expanded loop in the morning and afternoon, then runs a reduced service in the middle of the day. Under the proposed structure, this route layout would be discontinued. There were also requests for additional service on the 11 Ruby, which currently only runs during peak hours. The 11 Philo is proposed to run full weekday service, and otherwise replaced by the Northeast Connect demand-response zone.

Finally, there were requests for expanded service in Savoy, the village south of Champaign. This is not currently reflected in the proposed route structure, due to an agreement with the Village not to expand service until 2033.

## Metrics for Success

### Key Geographic Areas and Destination Types

Specific neighborhoods have been identified for more thorough analysis as the Access Scores are calculated for each stage of changes, as shown below. These neighborhoods fall into two basic categories: low-income neighborhoods (where vulnerable populations are more likely to rely on transit for basic accessibility) and fringe neighborhoods towards the edge of Champaign-Urbana’s urban area (which are more difficult serve and are generally more affected by the streamlined approach to the new route design). These two categories can overlap, so a neighborhood in one group may be relevant to another.

<image src="route_analysis_neighborhoods.png"
  attr="Route Analysis Neighborhoods"
  position="full">

The low-income neighborhoods include Garden Hills, Dobbins Downs, Holiday Park, Scottswood, and the Kinch neighborhood, all of which are lower-income, predominantly Black neighborhoods. Northeast Urbana has a collection of neighborhoods north of the interstate that include three mobile home parks. Finally, Ivanhoe Estates is a mobile home park being evaluated separately from the rest of Northeast Urbana, because it is located south of the interstate, so the service and accessibility challenges are inherently different, as well as the demographics of the area, populated largely by Mexican and Guatemalan immigrants.

The fringe neighborhoods include Ashland Park, Boulder Ridge, Sawgrass, Cherry Hills, and the larger Southwest Champaign block, including the Copper Ridge, Ironwood, and Trails at Brittany subdivisions. These are all generally higher income, low-density, purely residential neighborhoods located on the far ends of the community. Residents are less likely to be regular transit users and less likely to be considered vulnerable populations. The scores for these outer neighborhoods serve as metrics for route coverage and accessibility. In the central core of the community, where pedestrian Access Scores are generally higher, transit has less of an impact on the Access Score. By comparison, these outer neighborhoods with less pedestrian access will more meaningfully show the accessibility impacts of public transit.

### Accessibility and Equity Goals

GTFS feeds will be generated for each of the route scenarios listed above, then used to calculate new Access Scores for each scenario. All of the street segments within each neighborhood will have their scores for each resource type averaged together, creating a mean score for the neighborhood.

For transit, an Access Score can increase if a route is connecting community members to a new resource, or if they are reaching existing resources more quickly. Using the formula in the model with one hour as the maximum travel time, a change of twenty points equals roughly twelve minutes of travel time. That is the threshold that will be considered as a substantial change to travel.

Any score that increases by more than twenty will be considered especially improved, with a decrease of twenty being considered especially worsened.

Using the Access Score in this way provides an especially valuable insight into the route analysis. Transit agencies often choose to have more direct routes with greater walking distances (as Nelson Nygaard has generally suggested) or routes that serve neighborhoods more intensely with longer trip times (as the routes are currently designed). Increased walking distance has a negative impact on the score, where  shorter trips have a positive impact, with the same being true in the inverse. The Access Score is able to take both of those factors into consideration to determine whether the intended benefits of each paradigm are actually being achieved.

Outside of the Access Score, the analysis will also consider other factors, such as average trip time, average walking distance to the nearest bus stop, and the number of bus stops within a quarter-mile of a bus route. As the analysis proceeds and trends can be identified, additional performance metrics will be identified and recorded as needed.



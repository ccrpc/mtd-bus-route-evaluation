# Sustainable Neighborhoods Bus Route Evaluation for the Champaign-Urbana Mass Transit District

## Repository Setup
- Fork this repository.
- Clone the forked repository.
- In a text editor, find and replace all occurrences of `mtd-bus-route-evaluation`
  with the project slug (last part of the URL) of your forked repository.
- Find and replace all occurrences of `Sustainable Neighborhoods Bus Route Evaluation for the Champaign-Urbana Mass Transit District` with the
  human-readable name of your plan.
- Customize the directory names and text of the pages in the `content`
  directory.
- Commit and push the changes.

## Theme Installation
From the Terminal menu in VS Code, choose Run Task. Then select `Update theme`.

## Development
From the Terminal menu in VS Code, choose Run Task. Then select `Run server`.
